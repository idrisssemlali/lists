
class Node {
    public String data;
    public Node next;
    public Node prev;

    public void displayNodeData() {
        System.out.println(data);
    }
}

class DoublyLinkedList {

    private Node head;
    private Node tail;
    int size;

    public boolean isEmpty() {
        return (head == null);
    }


    public void insertFirst(String data) {
        Node pnoeud = new Node();
        pnoeud.data = data;
        pnoeud.next = head;
        pnoeud.prev=null;
        if(head!=null)
            head.prev=pnoeud;
        head = pnoeud;
        if(tail==null)
            tail=pnoeud;
        size++;
    }


    public void insertLast(String data) {
        Node dnoeud = new Node();
        dnoeud.data = data;
        dnoeud.next = null;
        dnoeud.prev=tail;
        if(tail!=null)
            tail.next=dnoeud;
        tail = dnoeud;
        if(head==null)
            head=dnoeud;
        size++;
    }

    public Node deleteFirst() {

        if (size == 0)
            throw new RuntimeException("La liste est vide");
        Node x = head;
        head = head.next;
        head.prev = null;
        size--;
        return x;
    }


    public Node deleteLast() {

        Node x = tail;
        tail = tail.prev;
        tail.next=null;
        size--;
        return x;
    }


    // Suppression d'un element  depuis un noeud donnée(Apès le noeud donné )
    public void deleteAfter(Node after) {
        Node x = head;
        while (x.next != null && x.data != after.data) {
            x = x.next;
        }
        if (x.next != null)
            x.next.next.prev=x;
        x.next = x.next.next;

    }


    public void printLinkedListForward() {
        System.out.println("Liste depuis le haut");
        Node current = head;
        while (current != null) {
            current.displayNodeData();
            current = current.next;
        }
        System.out.println();
    }


    public void printLinkedListBackward() {
        System.out.println("Liste depuis le bas ");
        Node current = tail;
        while (current != null) {
            current.displayNodeData();
            current = current.prev;
        }
        System.out.println();
    }
}




public class DoublyLinkedListExp {

    public static void main (String[] args){
        DoublyLinkedList Voiture = new DoublyLinkedList();
        Node noeud = new Node();

        noeud.data="Voiture2";


       Voiture.insertFirst("Voiture1");
       Voiture.insertFirst("Voiture2");
       Voiture.insertFirst("Voiture3");
       Voiture.insertLast("Voiture X");
       Voiture.printLinkedListForward();
       Voiture.deleteAfter(noeud);
       Voiture.printLinkedListForward();



    }


}
