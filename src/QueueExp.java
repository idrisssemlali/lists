import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class QueueExp {

    public static void main(String[] args) {

        Queue<String> ListeGarage = new LinkedList<>();


        ListeGarage.add("Voiture1");
        ListeGarage.add("Voiture2");
        ListeGarage.add("Voiture3");
        ListeGarage.add("Voiture4");
       // ListeGarage.remove();//suppression du premier element !!!
        ListeGarage.remove("Voiture2");// suppression de l'objet donné !!!
        ListeGarage.poll();//suppression du premier element!!! ( semblable a la methode remove();
        //ListeGarage.clear();
        for (String e: ListeGarage)
            System.out.println(e);


    }
}
